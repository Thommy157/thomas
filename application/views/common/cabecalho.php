<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Material Design for Bootstrap</title>
        <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
        <link rel="stylesheet" href="<?= base_url() ?>assets/mdb/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/mdb/css/mdb.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/mdb/css/style.css">
    <style>
         body {
            background-color: #2e2e2e;
        }
        .text-poppy-red {
            color: #ef5350 !important;
        }
        .btn-poppy-red {
            background-color: #ef5350 !important;
        }
        .bg-poppy-red {
            background-color: #ef5350 !important;
        }
    </style>
</head>
<body>
          