<?php
defined('BASEPATH') OR exit('No direct script acess allowed');

class Home extends CI_Controller {
    
    public function index(){
        $this->load->view('common/cabecalho');
        $this->load->view('common/navbar');
        $this->load->view('common/rodape');

    }
}